Cp2::Application.routes.draw do
  resources :points do
    collection do
      get 'range'
    end
  end
 

  resources :jobs

  resources :distance_matrix_entries
  match 'distance_matrix_entries/:id/polyline', to: 'distance_matrix_entries#get_polyline', :via => :get
  match 'distance_matrix_entries/:id', to: 'distance_matrix_entries#update_tag', :via => :post
  resources :distance_matrices

  resources :tracksegments #, :only => [:index, :show]


  resources :tracks, :except => [:new]

  match '/explore' => 'root#explore', :via => :get
  match '/search' => 'root#search', :via => :get
  match '/heatmap' => 'root#heatmap', :via => :get
  match '/api/completeJob' => 'api#completeJob', :via => :post
  match '/api/getJobs' => 'api#getJobs', :via => :get
  match '/api/reset' => 'api#reset', :via => :get  

  get '/tracksegments/:id/points', to: 'tracksegments#tracksegment_points'
  get '/tracksegments/:id/points/index', to: 'tracksegments#find_point'

  root 'root#index'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
