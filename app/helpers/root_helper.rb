module RootHelper

  def track_id_to_js(id)
    content_tag(:script, :type => "text/javascript") do
      "var js_track_id = "+id.to_s;
    end
  end

end
