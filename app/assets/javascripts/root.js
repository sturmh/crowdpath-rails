// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


.navbar-search .search-query:focus, .navbar-search .search-query.focused {
    padding-left: 30px;
    background-position: 13px 9px;
}