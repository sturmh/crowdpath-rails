// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var map;
var polylines = [];
var markers = [];

function onchange() {

}

function loadPoints(start_id, end_id) {
  points = [];

   $.getJson = function(url,data){
    return $.ajax({
        contentType: "application/json",
        url: url,
        data: data || {},
        type: "GET",
        async: false,
        dataType: "json"
    });
  }
  
  $.getJson("/points/range?start="+ start_id +"&end="+ end_id).done(
    function(data){
      points = data;         
    }).fail(function(){
      console.log(arguments);
    }
  );

  reformatted_points = [];
  for(var i = 0; i < points.length; ++i) {
    gp = new google.maps.LatLng(points[i].lat, points[i].lon);
    reformatted_points.push(gp);
  }  
  console.log(reformatted_points);
  return reformatted_points;
}

function drawPolyline(points) {
  console.log("drawing polyline");    

  for(var i = 0; i < polylines.length; ++i) {
    polylines[i].setMap(null);
  }
  polylines = [];

  for(var i = 0; i < markers.length; ++i) {
    markers[i].setMap(null);
  }
  markers = [];

  var polyline = new google.maps.Polyline({
      path: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.75,
      strokeWeight: 5,
      map: map
   });    

  var start = points[0];
  var end = points[points.length-1];

  var bounds = new google.maps.LatLngBounds();
  bounds.extend(start);
  bounds.extend(end);
  map.fitBounds(bounds);

  placeMarker(start, 'blue');
  placeMarker(end, 'red');

  polylines.push(polyline);
}

function buildHeatmap(points) {
   var polyline = new google.maps.Polyline({
      path: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.15,
      strokeWeight: 5,
      map: map
   });     

  var start = points[0];
  var end = points[points.length-1];

  var bounds = new google.maps.LatLngBounds();
  bounds.extend(start);
  bounds.extend(end);
  map.fitBounds(bounds);
}


function initializeMap() {  
  var map_center = new google.maps.LatLng(44.96, -93.255);
  var map_options = { zoom: 12, center: map_center, mapTypeId: google.maps.MapTypeId.ROADMAP };
  console.log(map);
  map = new google.maps.Map(document.getElementById("map-canvas"), map_options);  

  $(window).resize(function () {
  
    var h = $(window).height(), offsetTop = 80; // Calculate the top offset
 
    $('#map-canvas').css('height', (h - offsetTop));  
                                                  
  }).resize()


}


function displayOnMap(data, map) {
  console.debug(data);
}

function placeMarkerLatLng(lat, lng, color)  {  
  var start = new google.maps.LatLng(lat, lng);
  placeMarker(start, color); 
  
}

function placeMarker(point, color)  {  
   var marker = new google.maps.Marker({
    position: point, 
    map: map});
    
    if(color == 'red') {
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'; 
    } else if(color == 'blue') {
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
    } else if(color == 'green') {
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
    } else {
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
    }
    
    marker.setIcon(iconFile)
    markers.push(marker);  
}

function loadTrackPolyline(polyline, color) {  
  if (color == null || color == undefined) {
    color = "#FF0000";
  }
  console.log(polyline);
  decoded_path = google.maps.geometry.encoding.decodePath(polyline);
  console.log(decoded_path);
  path_options = { path: decoded_path, strokeColor: color, strokeOpacity: 0.75, strokeWeight: 3};
  track_path = new google.maps.Polyline(path_options);
  track_path.setMap(map);
  polylines.push(track_path); 
  var start = new google.maps.LatLng(decoded_path[0].lat(), decoded_path[0].lng());
  var end = new google.maps.LatLng(decoded_path[decoded_path.length-1].lat(), decoded_path[decoded_path.length-1].lng());

  var bounds = new google.maps.LatLngBounds();
  bounds.extend(start);
  bounds.extend(end);
  map.fitBounds(bounds);

  placeMarker(start, 'blue');
  placeMarker(end, 'red');
}

function loadTrack(id, color) {
  var polyline;

  $.getJson = function(url,data){
    return $.ajax({
        contentType: "application/json",
        url: url,
        data: data || {},
        type: "GET",
        async: false,
        dataType: "json"
    });
  }
  
  $.getJson("/tracksegments/" + id + ".json").done(
    function(data){
      polyline = data.polyline;
      console.log(polyline);
    }).fail(function(){
      console.log(arguments);
    }
  );

  decoded_path = google.maps.geometry.encoding.decodePath(polyline);
  path_options = { path: decoded_path, strokeColor: color, strokeOpacity: 0.75, strokeWeight: 3};
  track_path = new google.maps.Polyline(path_options);
  track_path.setMap(map);

  
  var start = new google.maps.LatLng(decoded_path[0].lat(), decoded_path[0].lng());
  var end = new google.maps.LatLng(decoded_path[decoded_path.length-1].lat(), decoded_path[decoded_path.length-1].lng());

  var bounds = new google.maps.LatLngBounds();
  bounds.extend(start);
  bounds.extend(end);
  map.fitBounds(bounds);

  placeMarker(start, 'green');
  placeMarker(end, 'green');
}


