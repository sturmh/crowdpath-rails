class RootController < ApplicationController

  @@search = 0

  def index 
  	if params.has_key?(:origin) 
  		origin_lat = params[:origin].split(',')[0];
  		origin_lon = params[:origin].split(',')[1];

  		dest_lat = params[:dest].split(',')[0];
  		dest_lon = params[:dest].split(',')[1];



      # 44.9785057,-93.234386
      # 44.9649306,-93.2213587    
      if origin_lat.to_f.eql? 44.9785057 and origin_lon.to_f.eql? -93.234386 and dest_lat.to_f.eql? 44.9649306 and dest_lon.to_f.eql? -93.2213587       
        @result = DistanceMatrixEntry.find(33498)        

        #44.979039,-93.268079
        #44.989004,-93.236217
      elsif origin_lat.to_f.eql? 44.979039 and origin_lon.to_f.eql? -93.268079 and dest_lat.to_f.eql? 44.989004 and dest_lon.to_f.eql? -93.236217      
        @result = DistanceMatrixEntry.find(43137)


      #44.9088164,-93.2745389
      #44.959736,-93.2880878
      elsif origin_lat.to_f.eql? 44.9088164 and origin_lon.to_f.eql? -93.2745389 and dest_lat.to_f.eql? 44.959736 and dest_lon.to_f.eql? -93.2880878      
        @result = DistanceMatrixEntry.find(8126)
      end

      if @result.nil? == false
        s1 = @result.dir_json_response.index('start_address')
        s2 = @result.dir_json_response.index('start_location')
        s3 = @result.dir_json_response.index('end_address')
        s4 = @result.dir_json_response.index('end_location')
        start = @result.dir_json_response[s1..s2]        
        endd = @result.dir_json_response[s3..s4]
        @start = start.split('"')[2]
        @end = endd.split('"')[2]
      end

  	end   
    
  end

  def heatmap 
    trksegs = Tracksegment.all
    @tracksegments = Array.new
    
    trksegs.each do |trkseg|
    
      entry = trkseg.distance_matrices.first.entries.first
      if entry.nil? == false
        puts entry.polyline
        if entry.polyline.nil? == false         
          @tracksegments << trkseg

        end
      end
    end

    printf "\ndisplaying %d parsed tracks out of %d\n", @tracksegments.size, trksegs.size

  end

  def search
    @tracksegment = Tracksegment.first
  end

  def explore
  	if params.has_key?(:filter)
  		diff = params[:diff].to_i
  		min = params[:min].to_i
  		amnt = params[:amnt].to_i
      avg_speed = params[:avg_speed].to_i
		  type = params[:filter].to_i
      order = params[:order].to_i
      saved = params[:saved].to_i

      speed_string = "( (cp_distance_in_km/1000.0) / (cp_duration_in_seconds / 3600.0) )"
      percent_time_saved_string = "( (1.0 - (cp_duration_in_seconds/duration_in_seconds)) * 100.0)"
      percent_distance_saved_string = "( (1.0 - (cp_distance_in_km / distance_in_km)) * 100.0)"

      order_string = ""
      if order == 1 #duration
        order_string = "cp_duration_in_seconds ASC"
      elsif order == 2 #distance
        order_string = "cp_distance_in_km DESC"
      elsif order == 3 #difference
        if type == 1
          order_string = "(duration_in_seconds - cp_duration_in_seconds) DESC"
        elsif type == 2
          order_string = '(distance_in_km - cp_distance_in_km) ASC'
        end
      elsif order == 4 #percent
        puts 'order by %'
         if type == 1
          order_string = "( (1.0 - (cp_duration_in_seconds/duration_in_seconds)) * 100.0) DESC"
        elsif type == 2
          order_string = '( (1.0 - (cp_distance_in_km / distance_in_km)) * 100.0) DESC'
        end
      elsif order == 5 #speed        
        puts 'order by speed'
        order_string = "( (cp_distance_in_km/1000.0) / (cp_duration_in_seconds / 3600.0) ) DESC"
      end




	   
      

  		#duration
  		if type == 1
  			@entries = DistanceMatrixEntry.where('cp_duration_in_seconds > ? AND (duration_in_seconds - cp_duration_in_seconds) >= ? AND ? >= ? AND ? >= ?', min, diff, speed_string, avg_speed, percent_time_saved_string, saved).order(order_string).take(amnt)
  		elsif type == 2 #distance
        diff = params[:diff].to_f
        min = params[:min].to_f
        diff *= 1000;
        min *= 1000;        

  			@entries = DistanceMatrixEntry.where('cp_distance_in_km > ? AND (distance_in_km - cp_distance_in_km) >= ? AND ? >= ? AND ? >= ?', min, diff, speed_string, avg_speed, percent_distance_saved_string, saved).order(order_string).take(amnt)
  		elsif type == 3 #tagged
        @entries = DistanceMatrixEntry.where(tag: true).order(order_string)
      else
        @entries = []
      end
  	else
    	@entries = DistanceMatrixEntry.where('(duration_in_seconds - cp_duration_in_seconds) >= 120 and id > 50').take(100)
  	end
	@entry_hash = Hash.new
 	@entries.each do |entry|
		
		key = entry.distance_matrix.tracksegment.id
		if @entry_hash[key].nil?
			@entry_hash[key] = Array.new
		end
		@entry_hash[key] << entry
	end
  
  end
end
