class ApiController < ApplicationController  
  # api/getJobs
  def getJobs
    if(!params.has_key?(:numJobs) or !params.has_key?(:workerId))
      render :json => '{ "error":1, "message":"missing param" }'
      return    
    else 
      numJobs = params[:numJobs]
      workerId = params[:workerId]

      jobs = Job.where(pending: false, processed: false).take(numJobs)
      jobs.each do |job|
        puts 'updating job'
        job.update(:pending => true)
        job.update(:worker_id => workerId)
      end

      render :json => jobs.to_json
    end
  end

  def reset
    if params.has_key? :id
      j = Job.find(params[:id])
      if j.nil?
        render :json => '{ "error":47, "message":"id not found }'
        return
      end
      dm = j.distance_matrix_entry
      dm.update(:polyline => nil)
      dm.update(:distance_in_km => 0)
      dm.update(:duration_in_seconds => 0)
      dm.update(:json_response => nil)
      dm.update(:dir_json_response => nil)

      j.update(:pending => false)
      j.update(:processed => false)
      j.update(:worker_id => nil)
    else
      jobs = Job.where(pending: true, processed: false)
      jobs.each do |job|
        job.update(:pending => false)
        job.update(:worker_id => nil)
      end
    end

    render :json => "{'reset':'true'}"
  end

  def completeJob    
    workerId = params[:workerId]    
    jobId = params[:id]
    duration = params[:duration]
    distance = params[:distance]
    polyline = params[:polyline]
    json = params[:json_response]
    dir_json = params[:dir_json_response]
    queryTime = params[:queryTime]

    puts 'workerId'
    puts workerId.nil?
    puts 'jobId'
    puts jobId.nil?
    puts 'dur'
    puts duration.nil?
    puts 'dist'
    puts distance.nil?
    puts 'json'
    puts json.nil?
    puts 'time'
    puts queryTime.nil?
    puts 'polyline'
    puts polyline.nil?
    #check if have all parameter
    if workerId.nil? or jobId.nil? or duration.nil? or distance.nil? or polyline.nil? or json.nil? or queryTime.nil?
      render :json => '{ "error":1, "message":"missing param" }'
      return
    end

    #find job
    job = Job.where(pending: true, processed: false, id: jobId, worker_id: workerId).take(1).first    
    if job.nil?
      render :json => '{"error":2, "message":"Could not find job"}'
      return
    end

    job.pending = false
    job.processed = true
    job.completed_at = DateTime.strptime(queryTime.to_s,'%s')  
    job.distance_matrix_entry.duration_in_seconds = duration
    job.distance_matrix_entry.distance_in_km = distance
    job.distance_matrix_entry.polyline = polyline
    job.distance_matrix_entry.json_response = json.to_s
    job.distance_matrix_entry.dir_json_response = dir_json.to_s
    job.save
    job.distance_matrix_entry.save
    render :json => '{ "success":"true" }'
  end
end
