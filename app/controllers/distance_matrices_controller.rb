class DistanceMatricesController < ApplicationController
  before_action :set_distance_matrix, only: [:show, :edit, :update, :destroy]

  # GET /distance_matrices
  # GET /distance_matrices.json
  def index
    @distance_matrices = DistanceMatrix.all
  end

  # GET /distance_matrices/1
  # GET /distance_matrices/1.json
  def show        
  end

  # GET /distance_matrices/new
  def new
    @distance_matrix = DistanceMatrix.new
  end

  # GET /distance_matrices/1/edit
  def edit
  end

  # POST /distance_matrices
  # POST /distance_matrices.json
  def create
    @distance_matrix = DistanceMatrix.new(distance_matrix_params)

    respond_to do |format|
      if @distance_matrix.save
        format.html { redirect_to @distance_matrix, notice: 'Distance matrix was successfully created.' }
        format.json { render action: 'show', status: :created, location: @distance_matrix }
      else
        format.html { render action: 'new' }
        format.json { render json: @distance_matrix.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /distance_matrices/1
  # PATCH/PUT /distance_matrices/1.json
  def update
    respond_to do |format|
      if @distance_matrix.update(distance_matrix_params)
        format.html { redirect_to @distance_matrix, notice: 'Distance matrix was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @distance_matrix.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /distance_matrices/1
  # DELETE /distance_matrices/1.json
  def destroy
    @distance_matrix.destroy
    respond_to do |format|
      format.html { redirect_to distance_matrices_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_distance_matrix
      @distance_matrix = DistanceMatrix.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def distance_matrix_params
      params[:distance_matrix]
    end
end
