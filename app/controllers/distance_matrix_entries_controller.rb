class DistanceMatrixEntriesController < ApplicationController
  before_action :set_distance_matrix_entry, only: [:get_polyline, :show, :edit, :update, :destroy, :update_tag]

  # GET /distance_matrix_entries
  # GET /distance_matrix_entries.json
  def index    
    if params.has_key?(:id)
      redirect_to '/distance_matrix_entries/'.concat(params[:id])    
      return;
    end
    @distance_matrix_entries = DistanceMatrixEntry.find(:all, :limit => 300)
  end

  # GET /distance_matrix_entries/1
  # GET /distance_matrix_entries/1.json
  def show    
  end

  # GET /distance_matrix_entries/1/polyline
  def get_polyline
    render :json => @distance_matrix_entry.polyline
  end

  # GET /distance_matrix_entries/new
  def new
    @distance_matrix_entry = DistanceMatrixEntry.new
  end

  # GET /distance_matrix_entries/1/edit
  def edit
  end

  # POST /distance_matrix_entries
  # POST /distance_matrix_entries.json
  def create
    @distance_matrix_entry = DistanceMatrixEntry.new(distance_matrix_entry_params)

    respond_to do |format|
      if @distance_matrix_entry.save
        format.html { redirect_to @distance_matrix_entry, notice: 'Distance matrix entry was successfully created.' }
        format.json { render action: 'show', status: :created, location: @distance_matrix_entry }
      else
        format.html { render action: 'new' }
        format.json { render json: @distance_matrix_entry.errors, status: :unprocessable_entity }
      end
    end
  end


  def update_tag
    @distance_matrix_entry.tag = params[:tag]    
    @distance_matrix_entry.save

    respond_to do |format|      
      format.html { redirect_to :back, notice: 'Distance matrix entry was successfully updated.' }      
    end
  end
  # PATCH/PUT/Post /distance_matrix_entries/1
  # PATCH/PUT /distance_matrix_entries/1.json
  def update    
    @distance_matrix_entry.tag = params[:distance_matrix_entry][:tag]    
    @distance_matrix_entry.save

    respond_to do |format|      
      format.html { redirect_to @distance_matrix_entry, notice: 'Distance matrix entry was successfully updated.' }
      format.json { head :no_content }
    end
  end

  # DELETE /distance_matrix_entries/1
  # DELETE /distance_matrix_entries/1.json
  def destroy
    @distance_matrix_entry.destroy
    respond_to do |format|
      format.html { redirect_to distance_matrix_entries_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_distance_matrix_entry
      @distance_matrix_entry = DistanceMatrixEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def distance_matrix_entry_params
      params[:distance_matrix_entry]      
    end
end
