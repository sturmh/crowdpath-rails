class TracksegmentsController < ApplicationController
  before_action :set_tracksegment, only: [:show, :edit, :update, :destroy, :find_point, :tracksegment_points]

  # GET /tracksegments
  # GET /tracksegments.json
  def index
    
    @tracksegments = Tracksegment.all
  end

  # GET /tracksegments/1
  # GET /tracksegments/1.json
  def show
     @entry = DistanceMatrixEntry.where('origin_id = ? and dest_id = ?', @tracksegment.points.first.id, @tracksegment.points.last.id).take(1).first

    respond_to do |format|
      format.html
      format.json { render :json => @tracksegment.to_json(:methods => [:polyline]) }
    end
  end

  # GET /tracksegments/new
  def new
    @tracksegment = Tracksegment.new
   
  end

  # GET /tracksegments/1/edit
  def edit
  end

  # POST /tracksegments
  # POST /tracksegments.json
  def create
    @tracksegment = Tracksegment.new(tracksegment_params)

    respond_to do |format|
      if @tracksegment.save
        format.html { redirect_to @tracksegment, notice: 'Tracksegment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @tracksegment }
      else
        format.html { render action: 'new' }
        format.json { render json: @tracksegment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tracksegments/1
  # PATCH/PUT /tracksegments/1.json
  def update    
    respond_to do |format|
      if @tracksegment.update(tracksegment_params)
        format.html { redirect_to @tracksegment, notice: 'Tracksegment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tracksegment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tracksegments/1
  # DELETE /tracksegments/1.json
  def destroy
    @tracksegment.destroy
    respond_to do |format|
      format.html { redirect_to tracksegments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tracksegment
      @tracksegment = Tracksegment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tracksegment_params
      params[:tracksegment]
    end
end
