class TracksController < ApplicationController
  before_action :set_track, only: [:show, :edit, :update, :destroy]

  # GET /tracks
  # GET /tracks.json
  def index
    @tracks = Track.all
  end

  # GET /tracks/1
  # GET /tracks/1.json
  def show
    @track = Track.find(params[:id])
  end

  # GET /tracks/new
  def new
   # i dont use this..instead import a gpx file    
  end

  # GET /tracks/1/edit
  def edit
  end

  # POST /tracks
  # POST /tracks.json
  def create    
    file = params[:uploaded_file]    

    doc = Nokogiri::XML(file.read)
    
    jobs = []  
    trks = parse_gpx_file(doc.root)
    trks.each do |trk|
      trk.tracksegments.each do |trkseg|
        jobs.concat build_distance_matrix(trkseg)
      end     
      if trk.tracksegments.size == 0
        next
      end
      trk.save            

      Job.transaction do       
        jobs.each(&:save!)
      end      
      
    end
    
    respond_to do |format|
      format.html { render action: 'index' }      
    end
  end

  # PATCH/PUT /tracks/1
  # PATCH/PUT /tracks/1.json
  def update
    respond_to do |format|
      if @track.update(track_params)
        format.html { redirect_to @track, notice: 'Track was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tracks/1
  # DELETE /tracks/1.json
  def destroy
    first = @track.tracksegments.first.distance_matrices.first.entries.first
    last = @track.tracksegments.last.distance_matrices.last.entries.last
    if last.nil? == false and first.nil? == false
      Job.where('id >= ? and id <= ?', first.id, last.id).destroy_all
    end
    @track.destroy
    respond_to do |format|
      format.html { redirect_to tracks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_track
      @track = Track.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def track_params
      params.require(:track).permit(:name)
    end

    def self.haversine(lat1, long1, lat2, long2)      
      dtor = Math::PI/180
      r = 6378.14*1000
     
      rlat1 = lat1 * dtor 
      rlong1 = long1 * dtor 
      rlat2 = lat2 * dtor 
      rlong2 = long2 * dtor 

      dlon = rlong1 - rlong2
      dlat = rlat1 - rlat2
     

      a = power(Math::sin(dlat/2), 2) + Math::cos(rlat1) * Math::cos(rlat2) * power(Math::sin(dlon/2), 2)
      c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))
      d = r * c
        
      return d
    end

    def self.power(num, pow)
      num ** pow
    end

    def parse_gpx_file(root)
      tracks = Array.new
      root.elements.each do |c|      
        if c.name.eql? 'trk'
          tracks << parse_trk(c)
          puts 'parsed track'
        end
      end
      return tracks
    end

    def parse_trk(node) 
      trk = Track.new
      node.elements.each do |c|
        if c.name.eql? 'name'
          trk.name = c.to_s
        elsif c.name.eql? 'trkseg' and c.elements.first.elements.size > 0
          trk.tracksegments << parse_trkseg(c)
          printf "parsed trackseg (points:%d)\n", trk.tracksegments.last.points.size
        end
      end    
      return trk
    end

    def parse_trkseg(node) 
      trkseg = Tracksegment.new
      node.elements.each do |c| 
        if !c.name.eql? 'trkpt'
          next
        end
        trkseg.points << parse_trkpt(c);
      end
      return trkseg
    end

    def parse_trkpt(node) 
      pt = Point.new
      pt.lat = node.attr('lat')
      pt.lon = node.attr('lon')
      pt.recorded_at = node.elements.first.to_s
      return pt                
    end

    def build_distance_matrix(trkseg) 
      
      debug = false
      puts 'building distance matrix'
      jobs = Array.new
      matrix = DistanceMatrix.new
      matrix.interval_in_seconds = 60;
      points = trkseg.points

      if debug     
        points.each_with_index do |p, index|
          puts index.to_s.concat(") ").concat(p.to_s)
        end
      end

      pt_indices = Array.new
      pt_indices << 0
      prev_pt = points.first
      for i in 1..points.size-1 
        diff = points[i].recorded_at - prev_pt.recorded_at
        isLast = points.last == points[i]
        if diff > matrix.interval_in_seconds or isLast
          pt_indices << i  
          prev_pt = points[i];      
        end
      end

      if debug 
        puts "["
        pt_indices.each do |p|
          printf "\t{%d: %s} \n", p, points[p].to_s
        end
        puts "]"
      end

      puts 'got points -- creating entries'
      

      jobs = []
      for i in 0..pt_indices.size-2   
        puts i/pt_indices.size.to_f.round(2)    
        for k in i+1..pt_indices.size-1
          pts = points[pt_indices[i]..pt_indices[k]]

          if debug  
            puts "["
            printf "i:%d to k:%d", pt_indices[i], pt_indices[k]               
            pline_pts = (pts.map{ |p| [p.lat, p.lon] })

            if pline_pts.size == pts.size            
              pts.each_with_index do |pt, p|
                pline_pt = pline_pts[p]
                if pline_pt[0] == pt.lat and pline_pt[1] == pt.lon
                  printf "\t{%d: %s} \n", p+pt_indices[i], pt.to_s            
                else
                  puts 'shit'
                end
              end
              puts                                  
              puts "]"
            else 
              puts
              printf "%d vs %d\n", pts.size, pline_pts.size
            end
          end

          entry = DistanceMatrixEntry.new
          entry.origin = pts.first
          entry.dest = pts.last
          entry.origin_index = i
          entry.dest_index = k

          prev = pts.first
          distance = 0
          duration = 0          
          for j in 0..pts.size-2
            duration += pts[j].recorded_at - prev.recorded_at
            distance += TracksController.haversine(prev.lat, prev.lon, pts[j].lat, pts[j].lon)                                  
            prev = pts[j]
          end

          entry.cp_distance_in_km = distance.round(1)
          entry.cp_duration_in_seconds = duration          

          matrix.entries << entry   
          jobs << Job.new(:distance_matrix_entry => entry)       
        end
      end
      printf "built distance matrix with %d entries\n", matrix.entries.size
      trkseg.distance_matrices << matrix      
      puts 'added matrix to tracksegment'    
      return jobs
    end
end
