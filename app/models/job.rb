class Job < ActiveRecord::Base
  belongs_to :distance_matrix_entry, :foreign_key => 'distance_matrix_entry_id', :dependent => :destroy

  def as_json(options={})
    super(:only => [:id], 
      :include => 
      { 
        :distance_matrix_entry => 
        { 
          :only => [],
          :include => 
          {
            :origin => {:only => [:lat, :lon]},
            :dest => {:only => [:lat, :lon]}
          }
        }
      }
    )
  end
end
