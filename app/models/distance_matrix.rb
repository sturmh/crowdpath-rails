class DistanceMatrix < ActiveRecord::Base
	has_many :entries, class_name: "DistanceMatrixEntry", :dependent => :destroy
  belongs_to :tracksegment
end
