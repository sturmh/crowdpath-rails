class Point < ActiveRecord::Base
  belongs_to :tracksegment  

  def as_json(options={})
    super(:only => [:lat, :lon])
  end
  def to_s 
  	return "[".concat(self.lat.to_s).concat(",").concat(self.lon.to_s).concat(",").concat(self.recorded_at.to_s).concat("]")
  end
  def latlng
    [self.lat ,self.lon]
  end
end
