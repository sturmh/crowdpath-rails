class Tracksegment < ActiveRecord::Base
	belongs_to :track
	has_many :points, :dependent => :destroy
  has_many :distance_matrices, :dependent  => :destroy


	def polyline
    Polylines::Encoder.encode_points(self.points.map(&:latlng))
  end
end
