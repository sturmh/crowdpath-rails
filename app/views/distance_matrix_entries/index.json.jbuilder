json.array!(@distance_matrix_entries) do |distance_matrix_entry|
  json.extract! distance_matrix_entry, 
  json.url distance_matrix_entry_url(distance_matrix_entry, format: :json)
end