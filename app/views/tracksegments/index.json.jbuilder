json.array!(@tracksegments) do |tracksegment|
  json.extract! tracksegment, 
  json.url tracksegment_url(tracksegment, format: :json)
end