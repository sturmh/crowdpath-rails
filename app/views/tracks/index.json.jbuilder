json.array!(@tracks) do |track|
  json.extract! track, :name
  json.url track_url(track, format: :json)
end