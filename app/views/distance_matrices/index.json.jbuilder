json.array!(@distance_matrices) do |distance_matrix|
  json.extract! distance_matrix, 
  json.url distance_matrix_url(distance_matrix, format: :json)
end