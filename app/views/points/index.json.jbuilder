json.array!(@points) do |point|
  json.extract! point, :lat, :lon, :timestamp, :tracksegment_id
  json.url point_url(point, format: :json)
end