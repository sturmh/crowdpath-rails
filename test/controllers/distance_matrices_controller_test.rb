require 'test_helper'

class DistanceMatricesControllerTest < ActionController::TestCase
  setup do
    @distance_matrix = distance_matrices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:distance_matrices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create distance_matrix" do
    assert_difference('DistanceMatrix.count') do
      post :create, distance_matrix: {  }
    end

    assert_redirected_to distance_matrix_path(assigns(:distance_matrix))
  end

  test "should show distance_matrix" do
    get :show, id: @distance_matrix
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @distance_matrix
    assert_response :success
  end

  test "should update distance_matrix" do
    patch :update, id: @distance_matrix, distance_matrix: {  }
    assert_redirected_to distance_matrix_path(assigns(:distance_matrix))
  end

  test "should destroy distance_matrix" do
    assert_difference('DistanceMatrix.count', -1) do
      delete :destroy, id: @distance_matrix
    end

    assert_redirected_to distance_matrices_path
  end
end
