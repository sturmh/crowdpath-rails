require 'test_helper'

class TracksegmentsControllerTest < ActionController::TestCase
  setup do
    @tracksegment = tracksegments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tracksegments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tracksegment" do
    assert_difference('Tracksegment.count') do
      post :create, tracksegment: {  }
    end

    assert_redirected_to tracksegment_path(assigns(:tracksegment))
  end

  test "should show tracksegment" do
    get :show, id: @tracksegment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tracksegment
    assert_response :success
  end

  test "should update tracksegment" do
    patch :update, id: @tracksegment, tracksegment: {  }
    assert_redirected_to tracksegment_path(assigns(:tracksegment))
  end

  test "should destroy tracksegment" do
    assert_difference('Tracksegment.count', -1) do
      delete :destroy, id: @tracksegment
    end

    assert_redirected_to tracksegments_path
  end
end
