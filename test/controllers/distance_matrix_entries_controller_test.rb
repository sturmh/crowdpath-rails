require 'test_helper'

class DistanceMatrixEntriesControllerTest < ActionController::TestCase
  setup do
    @distance_matrix_entry = distance_matrix_entries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:distance_matrix_entries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create distance_matrix_entry" do
    assert_difference('DistanceMatrixEntry.count') do
      post :create, distance_matrix_entry: {  }
    end

    assert_redirected_to distance_matrix_entry_path(assigns(:distance_matrix_entry))
  end

  test "should show distance_matrix_entry" do
    get :show, id: @distance_matrix_entry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @distance_matrix_entry
    assert_response :success
  end

  test "should update distance_matrix_entry" do
    patch :update, id: @distance_matrix_entry, distance_matrix_entry: {  }
    assert_redirected_to distance_matrix_entry_path(assigns(:distance_matrix_entry))
  end

  test "should destroy distance_matrix_entry" do
    assert_difference('DistanceMatrixEntry.count', -1) do
      delete :destroy, id: @distance_matrix_entry
    end

    assert_redirected_to distance_matrix_entries_path
  end
end
