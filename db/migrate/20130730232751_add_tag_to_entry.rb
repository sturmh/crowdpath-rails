class AddTagToEntry < ActiveRecord::Migration
  def change
  	add_column :distance_matrix_entries, :tag, :boolean, :default => false
  end
end
