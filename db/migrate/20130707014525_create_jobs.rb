class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t| 
      t.boolean :pending, :default => false
      t.boolean :processed, :default => false
      t.integer :worker_id
      t.datetime :completed_at
      t.integer :distance_matrix_entry_id
      t.timestamps
    end
  end
end
