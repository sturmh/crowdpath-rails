class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.float :lat
      t.float :lon
      t.datetime :recorded_at
      t.references :tracksegment, index: true
      t.timestamps
    end
  end
end
