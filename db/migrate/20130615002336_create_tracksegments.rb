class CreateTracksegments < ActiveRecord::Migration
  def change
    create_table :tracksegments do |t|
	  t.references :track      
      t.timestamps
    end
  end
end
