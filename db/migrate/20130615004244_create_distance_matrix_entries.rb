class CreateDistanceMatrixEntries < ActiveRecord::Migration
  def change
    create_table :distance_matrix_entries do |t|      
      t.references :distance_matrix, index: true        
      t.integer :duration_in_seconds, :default => 0
      t.float :distance_in_km, :default => 0
      t.float :cp_distance_in_km, :default => 0
      t.float :cp_duration_in_seconds, :default => 0
      t.string :cp_polyline
      t.string :json_response            
      t.string :polyline
      t.integer :origin_id
      t.integer :dest_id          
      t.integer :origin_index
      t.integer :dest_index
      t.timestamps
    end
  end
end
