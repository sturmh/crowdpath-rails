class AddDirJsonResponseToDistanceMatrixEntry < ActiveRecord::Migration
  def change
    add_column :distance_matrix_entries, :dir_json_response, :string
  end
end
