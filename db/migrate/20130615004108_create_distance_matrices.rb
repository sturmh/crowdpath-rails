class CreateDistanceMatrices < ActiveRecord::Migration
  def change
    create_table :distance_matrices do |t|
    	t.integer :interval_in_seconds
      t.references :tracksegment
      t.timestamps
    end
  end
end
