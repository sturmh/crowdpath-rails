# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130730232751) do

  create_table "distance_matrices", force: true do |t|
    t.integer  "interval_in_seconds"
    t.integer  "tracksegment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "distance_matrix_entries", force: true do |t|
    t.integer  "distance_matrix_id"
    t.integer  "duration_in_seconds",    default: 0
    t.float    "distance_in_km",         default: 0.0
    t.float    "cp_distance_in_km",      default: 0.0
    t.float    "cp_duration_in_seconds", default: 0.0
    t.string   "cp_polyline"
    t.string   "json_response"
    t.string   "polyline"
    t.integer  "origin_id"
    t.integer  "dest_id"
    t.integer  "origin_index"
    t.integer  "dest_index"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dir_json_response"
    t.boolean  "tag",                    default: false
  end

  add_index "distance_matrix_entries", ["distance_matrix_id"], name: "index_distance_matrix_entries_on_distance_matrix_id"

  create_table "jobs", force: true do |t|
    t.boolean  "pending",                  default: false
    t.boolean  "processed",                default: false
    t.integer  "worker_id"
    t.datetime "completed_at"
    t.integer  "distance_matrix_entry_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "points", force: true do |t|
    t.float    "lat"
    t.float    "lon"
    t.datetime "recorded_at"
    t.integer  "tracksegment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "points", ["tracksegment_id"], name: "index_points_on_tracksegment_id"

  create_table "tracks", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tracksegments", force: true do |t|
    t.integer  "track_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
